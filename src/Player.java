
public class Player {

    private char name;
    private int win, lose, draw;

    public Player(char name) {
        this.name=name;
        win = 0;
        lose = 0;
        draw = 0;
    }

    public void setName(char name) {
        this.name = name;
    }

    public char getName() {
        return name;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public void countWin() {
        win += 1;
    }

    public void countLose() {
        lose += 1;
    }

    public void countDraw() {
        draw += 1;
    }
}
