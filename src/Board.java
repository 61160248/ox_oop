
public class Board {

    static char[][] data;
    Player currentPlayer;
    Player o, x, win;

    public Board(Player o, Player x) {
        data = new char[][]{
            {'-', '-', '-'},
            {'-', '-', '-'},
            {'-', '-', '-'}};
        this.o = o;
        this.x = x;
        this.randomPlayer();
        System.out.println(currentPlayer.getName());
    }

    public boolean setRowCol(int row, int col) {
        if (data[row][col] == '-') {
            data[row][col] = currentPlayer.getName();
            return true;
        } else {
            return false;
        }
    }

    public void randomPlayer() {
        if (Math.round(Math.random()) == 0) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }

    }

    public boolean checkWin(Player p, int row, int col) {
        if (checkRow(row) || checkCol(col) || checkCross1() || checkCross2()) {
            win = p;
            if (p == o) {
                o.countWin();
                x.countLose();
            } else if (p == x) {
                x.countWin();
                o.countLose();
            } else {
                x.countDraw();
                o.countDraw();
            }
            return true;
        }
        return false;
    }

    public void swithTurn() {
        if (currentPlayer.getName() == 'O') {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public char[][] getData() {
        return data;
    }

    private boolean checkRow(int row) {
        return (data[row][0] == data[row][1]
                && data[row][1] == data[row][2]
                && data[row][2] == currentPlayer.getName());
    }

    private boolean checkCol(int col) {
        return (data[0][col] == data[1][col]
                && data[1][col] == data[2][col]
                && data[2][col] == currentPlayer.getName());
    }

    private boolean checkCross1() {
        return (data[0][0] == data[1][1]
                && data[1][1] == data[2][2]
                && data[2][2] == currentPlayer.getName());
    }

    private boolean checkCross2() {
        return (data[0][2] == data[1][1]
                && data[1][1] == data[2][0]
                && data[2][0] == currentPlayer.getName());
    }

    public static boolean checkDraw(int count) {
        if (count == 9) {
            return true;
        }
        return false;
    }

    public Player getWinner() {
        return win;
    }
}
